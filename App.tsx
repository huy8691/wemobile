/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
// import "react-native-gesture-handler";
import React from "react";
import type { PropsWithChildren } from "react";
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from "react-native/Libraries/NewAppScreen";
import { BottomTabNavigator } from "./src/navigation/bottomTab";
import { DrawerNavigator } from "./src/navigation/drawer";
import {
  HStack,
  Center,
  NativeBaseProvider,
  HamburgerIcon,
  InfoIcon,
  Container,
  Box,
} from "native-base";

type SectionProps = PropsWithChildren<{
  title: string;
}>;

function App(): JSX.Element {
  const isDarkMode = useColorScheme() === "dark";

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <NativeBaseProvider>
      <SafeAreaProvider>
        <NavigationContainer>
          <BottomTabNavigator />
          {/* <DrawerNavigator /> */}

          {/* <DrawerNavigator /> */}

          {/* <Drawer.Navigator>
            <Drawer.Screen name="Home" component={Home} />
            <Drawer.Screen name="ProductDetail" component={ProductDetail} />
          </Drawer.Navigator> */}
        </NavigationContainer>
      </SafeAreaProvider>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
  },
  highlight: {
    fontWeight: "700",
  },
});

export default App;
