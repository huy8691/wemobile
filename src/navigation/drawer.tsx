import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Home from "../screens/home";
import ProductDetail from "../screens/productDetail";
import { Text } from "react-native";

const Drawer = createDrawerNavigator();

export const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="ProductDetail" component={ProductDetail} />
    </Drawer.Navigator>
  );
};
