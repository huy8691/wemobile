import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../screens/home";
import ProductDetail from "../screens/productDetail";
import { MainStackNavigator } from "./stack";
import { DrawerNavigator } from "./drawer";
import { HamburgerIcon } from "native-base";

const Tab = createBottomTabNavigator();
export const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="Home"
        component={MainStackNavigator}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color, size }) => <HamburgerIcon name="home" />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={DrawerNavigator}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color, size }) => <HamburgerIcon name="home" />,
        }}
      />
    </Tab.Navigator>
  );
};
