import axios from 'axios';
import base64 from 'react-native-base64';

const username = "ck_eae4bbfb9e95c29e94b402ad21314ca155d7d452"
const password = "cs_de03128b897a9811baaa161abb84e43cf8503626"
const authHeader =  base64.encode(`${username}:${password}`);
const callAPI = axios.create({
  baseURL: 'https://ecommerce.ginpe.com/', 
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Authorization': `Basic ${authHeader}`
  },
});

callAPI.interceptors.response.use(
  (res: any) => {
    return res;
  },
  (err: any) => {
    // if (err.response && err.response.status === 403) {
    // }
    console.log("7777")
    return Promise.reject(err);
  },
);

export {callAPI};
