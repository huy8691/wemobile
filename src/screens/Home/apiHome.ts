import {AxiosResponse} from 'axios';
import {callAPI} from '../../api/jwt-axios';
import {ProductListDataResponseType} from './modelHome';

const getListProduct = (
  params?: object,
): Promise<AxiosResponse<ProductListDataResponseType>> => {
  return callAPI({
    url: '/wp-json/wc/v3/products/',
    method: 'GET',
    params: {
      ...params,
    },
  });
};

export {getListProduct};
