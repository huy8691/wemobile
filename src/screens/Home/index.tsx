import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";

import React, { useState, useEffect } from "react";
import { getListProduct } from "./apiHome";
import { HStack, Center, Box, Button, Flex, Image } from "native-base";
import axios from "axios";
import { NavigationContainer } from "@react-navigation/native";
// import { createStackNavigator } from "@react-navigation/stack";
import ProductDetail from "../productDetail";

// const StackNavigator = createStackNavigator();
const Home = ({ navigation }) => {
  const [stateRerender, setStateRerender] = useState([]);
  useEffect(() => {
    getListProduct()
      .then((res) => {
        console.log("data from list perm boundary", res.data);
        setStateRerender(res.data);
      })
      .catch((response) => {
        console.log("data from list perm boundary2", response);
        setStateRerender(false);
      });
    // axios
    //   .get(`https://ecommerce.ginpe.com/wp-json/wp/v2/posts/`)
    //   .then((res) => {
    //     const persons = res.data;
    //     console.log("eeeeee", persons);
    //   })
    //   .catch((error) => console.log(error));
  }, []);
  return (
    <ScrollView>
      <Box p={3}>
        <Flex direction="row" flexWrap="wrap" ml="-2" mr="-2">
          {stateRerender.length > 0 &&
            stateRerender?.map((item, index: number) => {
              return (
                <Box key={index} p="2" w="50%">
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ProductDetail", { id: item.id })
                    }
                  >
                    <Box
                      rounded="md"
                      overflow="hidden"
                      borderColor="coolGray.200"
                      borderWidth="1"
                      flex={1}
                      _dark={{
                        borderColor: "coolGray.600",
                        backgroundColor: "gray.700",
                      }}
                      _light={{
                        backgroundColor: "gray.70",
                      }}
                    >
                      <Image
                        source={{
                          uri: `${item?.images[0]?.src}`,
                        }}
                        alt="Alternate Text"
                        size="xl"
                        mb="2"
                        width="100%"
                      />
                      <Box p="2">
                        <Text>{item.name}</Text>
                      </Box>
                    </Box>
                  </TouchableOpacity>
                </Box>
              );
            })}
        </Flex>
        <Box alignItems="center">
          {stateRerender.length}
          <Button onPress={() => console.log("hello world")}>Click Me</Button>
        </Box>
      </Box>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({});
