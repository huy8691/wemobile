import { StyleSheet, View, Button, Text } from "react-native";
import React, { useEffect, useState } from "react";
import { getProductDetail } from "./apiProductDetail";
// import { Image } from "native-base";
import { Image, ScrollView } from "react-native";
import { Box } from "native-base";
import HTMLView from "react-native-htmlview";
import Swiper from "react-native-swiper";

const ProductDetail = ({ route, navigation }) => {
  const { id } = route.params;
  const [stateProductDetail, setStateProductDetail] = useState();
  useEffect(() => {
    getProductDetail({ id: id })
      .then((res) => {
        console.log("data from list perm boundary7", res.data);
        setStateProductDetail(res.data);
      })
      .catch((response) => {
        console.log("data from list perm boundary2", response);
      });
    // axios
    //   .get(`https://ecommerce.ginpe.com/wp-json/wp/v2/posts/`)
    //   .then((res) => {
    //     const persons = res.data;
    //     console.log("eeeeee", persons);
    //   })
    //   .catch((error) => console.log(error));
  }, [route]);

  useEffect(() => {
    return () => {
      setStateProductDetail(undefined);
    };
  }, []);
  return (
    <ScrollView>
      <Swiper height={240} loop={false}>
        {stateProductDetail?.images.length > 0 &&
          stateProductDetail?.images?.map((item, index: number) => {
            return (
              <Image
                key={index}
                // style={styles.red}
                source={{
                  uri: `${item?.src}`,
                }}
                resizeMode="contain"
                alt="Alternate Text"
                style={{ height: 240 }}
              />
            );
          })}
      </Swiper>
      <Box p={3}>
        <View>
          <Text style={styles.title}>{stateProductDetail?.name} </Text>
          {stateProductDetail?.description && (
            <HTMLView value={stateProductDetail?.description} />
          )}
        </View>
      </Box>
    </ScrollView>
  );
};

export default ProductDetail;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 20,
  },
});
