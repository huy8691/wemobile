import {AxiosResponse} from 'axios';
import {callAPI} from '../../api/jwt-axios';
import {ProductListDataResponseType} from './modelProductDetail';

const getProductDetail = (
  params: object,
): Promise<AxiosResponse<ProductListDataResponseType>> => {
  return callAPI({
    url: `/wp-json/wc/v3/products/${params?.id}`,
    method: 'GET',
  });
};

export {getProductDetail};
